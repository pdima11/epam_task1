package by.epam.task1.exception;

/**
 * Created by Dzmitry_Padvalnikau on 2/9/2016.
 */
public class DAOException extends Exception{
    private static final long serialVersionUID = 1L;

    public DAOException(String message) {
        super(message);
    }

    public DAOException(String message, Exception e) {
        super(message, e);
    }
}
