package by.epam.task1.domain;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Dzmitry_Padvalnikau on 2/9/2016.
 */
public class FullNewsTO implements Serializable{
    private static final long serialVersionUID = 1L;

    private News news;
    private Author author;
    private List<Tag> tags;
    private List<Comment> comments;

    public News getNews() {
        return news;
    }

    public void setNews(News news) {
        this.news = news;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public boolean addTag(Tag tag) {
        return tags.add(tag);
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public boolean addComment(Comment comment) {
        return comments.add(comment);
    }
}
