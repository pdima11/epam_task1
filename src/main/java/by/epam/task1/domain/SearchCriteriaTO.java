package by.epam.task1.domain;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Dzmitry_Padvalnikau on 2/9/2016.
 */
public class SearchCriteriaTO implements Serializable{
    private static final long serialVersionUID = 1L;

    private Author author;
    private List<Tag> tags;

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public boolean addTag(Tag tag) {
        return tags.add(tag);
    }
}
