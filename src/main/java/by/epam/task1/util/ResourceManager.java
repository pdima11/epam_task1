package by.epam.task1.util;

import java.util.ResourceBundle;

/**
 * Created by User on 03.11.2015.
 */
public class ResourceManager {
    private static final String PROPERTIES_FILE_PATH = "database";
    private static final ResourceBundle resource = ResourceBundle.getBundle(PROPERTIES_FILE_PATH);

    public static String valueOf(String key) {
        return resource.getString(key);
    }
}
