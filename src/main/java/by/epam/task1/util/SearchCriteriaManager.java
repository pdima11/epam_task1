package by.epam.task1.util;

import by.epam.task1.domain.Author;
import by.epam.task1.domain.SearchCriteriaTO;
import by.epam.task1.domain.Tag;

import java.util.List;

public class SearchCriteriaManager {
    private static final String START_SQL_QUERY = "SELECT * FROM (SELECT all_news.*, ROWNUM row_num " +
          "FROM (SELECT news.news_id, title, short_text, full_text, news.creation_date, modification_date " +
          "FROM news ";
    private static final String JOIN_WITH_AUTHORS_TABLE = " INNER JOIN news_authors" +
            " ON news.news_id = news_authors.news_id";
    private static final String JOIN_WITH_TAGS_TABLE = " INNER JOIN news_tags" +
            " ON news.news_id = news_tags.news_id";
    private static final String END_SQL_QUERY = "ORDER BY creation_date DESC) all_news" +
            " WHERE ROWNUM < ?) WHERE row_num >= ?";


    public static String generateSQLQuery(SearchCriteriaTO searchCriteria) {
        Author author = searchCriteria.getAuthor();
        List<Tag> tags = searchCriteria.getTags();

        StringBuilder sb = new StringBuilder();
        sb.append(START_SQL_QUERY);

        if (author != null) {
            sb.append(JOIN_WITH_AUTHORS_TABLE);
        }

        if (tags != null) {
            sb.append(JOIN_WITH_TAGS_TABLE);
        }

        String authorParam = getAuthorParametersForSearch(author);
        sb.append(authorParam);

        String tagsParam = getTagsParametersForSearch(tags);
        sb.append(tagsParam);

        sb.append(END_SQL_QUERY);

        return sb.toString();
    }

    private static String getAuthorParametersForSearch(Author author) {
        if (author == null) {
            return "";
        }

        StringBuilder sb = new StringBuilder();
        sb.append(" WHERE author_id = ");
        sb.append(author.getId());

        return sb.toString();
    }

    private static String getTagsParametersForSearch(List<Tag> tags) {
        if (tags == null) {
            return "";
        }

        StringBuilder sb = new StringBuilder();
        sb.append(" AND ( ");

        for (Tag tag: tags) {
            sb.append(" tag_id = ");
            sb.append(tag.getId());
            sb.append(" OR ");
        }

        int lastPositionOR = sb.lastIndexOf("OR");
        sb.delete(lastPositionOR, lastPositionOR + 2);
        sb.append(") ");

        return sb.toString();
    }

}