package by.epam.task1.service.impl;

import by.epam.task1.dao.NewsDAO;
import by.epam.task1.domain.Author;
import by.epam.task1.domain.News;
import by.epam.task1.domain.SearchCriteriaTO;
import by.epam.task1.domain.Tag;
import by.epam.task1.exception.DAOException;
import by.epam.task1.exception.ServiceException;
import by.epam.task1.service.NewsService;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * Created by Dzmitry_Padvalnikau on 2/15/2016.
 */
@Named
public class NewsServiceImpl implements NewsService {
    private static final int NUMBER_OF_ITEMS_PER_PAGE = 7;

    @Inject
    private NewsDAO newsDAO;


    @Override
    public Long create(News news) throws ServiceException {
        Long newsId = null;

        if (news == null) {
            throw new ServiceException("Null pointer for News");
        }

        try {
            newsId = newsDAO.create(news);
        } catch (DAOException e) {
            throw new ServiceException("Failed to create News", e);
        }

        return newsId;
    }

    @Override
    public News read(long newsId) throws ServiceException {
        News news = null;

        try {
            news = newsDAO.read(newsId);
        } catch (DAOException e) {
            throw new ServiceException("Failed to read News", e);
        }

        return news;
    }

    @Override
    public void update(News news) throws ServiceException {
        if (news == null) {
            throw new ServiceException("Null pointer for News");
        }

        try {
            newsDAO.update(news);
        } catch (DAOException e) {
            throw new ServiceException("Failed to update News", e);
        }
    }

    @Override
    public void delete(long newsId) throws ServiceException {
        try {
            newsDAO.delete(newsId);
        } catch (DAOException e) {
            throw new ServiceException("Failed to delete News", e);
        }
    }

    @Override
    public List<News> search(long pageNumber, SearchCriteriaTO searchCriteria) throws ServiceException {
        List<News> newsList;

        long endPosition = (pageNumber * NUMBER_OF_ITEMS_PER_PAGE) + 1;
        long startPosition = endPosition - NUMBER_OF_ITEMS_PER_PAGE;

        try {
            newsList = newsDAO.search(startPosition, endPosition, searchCriteria);
        } catch (DAOException e) {
            throw new ServiceException("Failed to ", e);
        }
        return newsList;
    }

    @Override
    public List<News> readNewsListSortedByComments(long pageNumber) throws ServiceException {
        List<News> newsList;

        long endPosition = (pageNumber * NUMBER_OF_ITEMS_PER_PAGE) + 1;
        long startPosition = endPosition - NUMBER_OF_ITEMS_PER_PAGE;

        try {
            newsList = newsDAO.readNewsListSortedByComments(startPosition, endPosition);
        } catch (DAOException e) {
            throw new ServiceException("Failed to read news sorted by comments", e);
        }

        return newsList;
    }

    @Override
    public List<News> readNewsListPerPage(long pageNumber) throws ServiceException {
        List<News> newsList;

        long endPosition = (pageNumber * NUMBER_OF_ITEMS_PER_PAGE) + 1;
        long startPosition = endPosition - NUMBER_OF_ITEMS_PER_PAGE;

        try {
            newsList = newsDAO.readNewsListPerPage(startPosition, endPosition);
        } catch (DAOException e) {
            throw new ServiceException("Failed to ", e);
        }

        return newsList;
    }

    @Override
    public long countAllNews() throws ServiceException {
        long numberOfNews = 0;

        try {
            numberOfNews = newsDAO.countAllNews();
        } catch (DAOException e) {
            throw new ServiceException("Failed to ", e);
        }

        return numberOfNews;
    }

    @Override
    public void createBindWithAuthor(News news, Author author) throws ServiceException {
        try {
            newsDAO.createBindNewsWithAuthor(news, author);
        } catch (DAOException e) {
            throw new ServiceException("Failed to ", e);
        }
    }

    @Override
    public void createBindWithTags(News news, List<Tag> tags) throws ServiceException {
        try {
            newsDAO.createBindNewsWithTags(news, tags);
        } catch (DAOException e) {
            throw new ServiceException("Failed to ", e);
        }
    }

    @Override
    public void deleteBindWithAuthor(long newsId) throws ServiceException {
        try {
            newsDAO.deleteBindNewsWithAuthor(newsId);
        } catch (DAOException e) {
            throw new ServiceException("Failed to ", e);
        }
    }

    @Override
    public void deleteBindWithTags(News news, List<Tag> tags) throws ServiceException {
        try {
            newsDAO.deleteBindNewsWithTags(news, tags);
        } catch (DAOException e) {
            throw new ServiceException("Failed to ", e);
        }
    }
}
