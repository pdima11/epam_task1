package by.epam.task1.service.impl;

import by.epam.task1.domain.*;
import by.epam.task1.exception.ServiceException;
import by.epam.task1.service.*;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dzmitry_Padvalnikau on 2/16/2016.
 */
@Named
public class FullNewsServiceImpl implements FullNewsService {

    @Inject
    private NewsService newsService;

    @Inject
    private AuthorService authorService;

    @Inject
    private TagService tagService;

    @Inject
    private CommentService commentService;


    @Override
    @Transactional(rollbackFor = { Exception.class })
    public void create(FullNewsTO fullNews) throws ServiceException {
        if (fullNews == null) {
            throw new ServiceException("Null pointer for FullNewsTO");
        }

        Long newsId = newsService.create(fullNews.getNews());
        fullNews.getNews().setId(newsId);
        newsService.createBindWithAuthor(fullNews.getNews(), fullNews.getAuthor());
        newsService.createBindWithTags(fullNews.getNews(), fullNews.getTags());
    }

    @Override
    @Transactional(rollbackFor = { Exception.class })
    public void delete(FullNewsTO fullNews) throws ServiceException {
        if (fullNews == null) {
            throw new ServiceException("Null pointer for FullNewsTO");
        }

        Long newsId = fullNews.getNews().getId();

        newsService.deleteBindWithAuthor(newsId);
        newsService.deleteBindWithTags(fullNews.getNews(), fullNews.getTags());
        commentService.deleteCommentsForNews(fullNews.getNews());
        newsService.delete(newsId);
    }

    @Override
    public FullNewsTO read(Long newsId) throws ServiceException {
        FullNewsTO fullNews = null;

        News news = newsService.read(newsId);
        Author author = authorService.readAuthorForNews(news);
        List<Tag> tags = tagService.readTagsForNews(news);
        List<Comment> comments = commentService.readCommentsForNews(news);

        fullNews = new FullNewsTO();

        fullNews.setNews(news);
        fullNews.setAuthor(author);
        fullNews.setTags(tags);
        fullNews.setComments(comments);

        return fullNews;
    }

    @Override
    public List<FullNewsTO> search(long pageNumber, SearchCriteriaTO searchCriteria) throws ServiceException {
        List<FullNewsTO> fullNewsList = new ArrayList<>();
        FullNewsTO fullNews = null;

        List<News> newsList = newsService.search(pageNumber, searchCriteria);

        for (News news: newsList) {
            Author author = authorService.readAuthorForNews(news);
            List<Tag> tags = tagService.readTagsForNews(news);
            List<Comment> comments = commentService.readCommentsForNews(news);

            fullNews = new FullNewsTO();
            fullNews.setNews(news);
            fullNews.setAuthor(author);
            fullNews.setTags(tags);
            fullNews.setComments(comments);

            fullNewsList.add(fullNews);
        }

        return fullNewsList;
    }

    @Override
    public List<FullNewsTO> readNewsList(long pageNumber) throws ServiceException {
        List<FullNewsTO> fullNewsList = new ArrayList<>();
        FullNewsTO fullNews = null;

        List<News> newsList = newsService.readNewsListPerPage(pageNumber);

        for (News news: newsList) {
            Author author = authorService.readAuthorForNews(news);
            List<Tag> tags = tagService.readTagsForNews(news);
            List<Comment> comments = commentService.readCommentsForNews(news);

            fullNews = new FullNewsTO();
            fullNews.setNews(news);
            fullNews.setAuthor(author);
            fullNews.setTags(tags);
            fullNews.setComments(comments);

            fullNewsList.add(fullNews);
        }

        return fullNewsList;
    }

    @Override
    public List<FullNewsTO> readNewsListSortedByComments(long pageNumber) throws ServiceException {
        List<FullNewsTO> fullNewsList = new ArrayList<>();
        FullNewsTO fullNews = null;

        List<News> newsList = newsService.readNewsListSortedByComments(pageNumber);

        for (News news: newsList) {
            Author author = authorService.readAuthorForNews(news);
            List<Tag> tags = tagService.readTagsForNews(news);
            List<Comment> comments = commentService.readCommentsForNews(news);

            fullNews = new FullNewsTO();
            fullNews.setNews(news);
            fullNews.setAuthor(author);
            fullNews.setTags(tags);
            fullNews.setComments(comments);

            fullNewsList.add(fullNews);
        }

        return fullNewsList;
    }
}
