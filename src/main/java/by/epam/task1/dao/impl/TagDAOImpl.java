package by.epam.task1.dao.impl;

import by.epam.task1.dao.TagDAO;
import by.epam.task1.domain.News;
import by.epam.task1.domain.Tag;
import by.epam.task1.exception.DAOException;
import by.epam.task1.util.ResourceManager;
import oracle.jdbc.proxy.annotation.Pre;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.inject.Inject;
import javax.inject.Named;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dzmitry_Padvalnikau on 2/12/2016.
 */
@Named
public class TagDAOImpl implements TagDAO {
    private static final String ORACLE_SQL_QUERY_CREATE = "oracle.create.tag";
    private static final String ORACLE_SQL_QUERY_READ = "oracle.read.tag";
    private static final String ORACLE_SQL_QUERY_UPDATE = "oracle.update.tag";
    private static final String ORACLE_SQL_QUERY_DELETE = "oracle.delete.tag";
    private static final String ORACLE_SQL_QUERY_DELETE_BIND_WITH_NEWS = "oracle.delete.bind.tag.with.news";
    private static final String ORACLE_SQL_QUERY_READ_FOR_NEWS = "oracle.read.tags.for.news";
    private static final String ORACLE_SQL_QUERY_READ_LIST = "oracle.read.tag.list";
    private static final String ORACLE_SQL_QUERY_READ_LIST_PER_PAGE = "oracle.read.tag.list.per.page";
    private static final String TAG_ID_ATTRIBUTE = "tag_id";

    @Inject
    private DataSource dataSource;

    @Override
    public Long create(Tag tag) throws DAOException {
        String sqlQuery = ResourceManager.valueOf(ORACLE_SQL_QUERY_CREATE);
        Long tagId = null;
        String generatedKeys[] = { TAG_ID_ATTRIBUTE };

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery, generatedKeys)) {

            statement.setString(1, tag.getName() );

            statement.executeUpdate();

            ResultSet resultSet = statement.getGeneratedKeys();

            if (resultSet.next()) {
                tagId = resultSet.getLong(1);
            }

            return tagId;
        } catch (SQLException e) {
            throw new DAOException("an error occurred while executing Tag create operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public Tag read(long tagId) throws DAOException {
        String sqlQuery = ResourceManager.valueOf(ORACLE_SQL_QUERY_READ);
        Tag tag = null;

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {

            statement.setLong(1, tagId);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                tag = new Tag();
                tag.setId(resultSet.getLong(1));
                tag.setName(resultSet.getString(2));
            }

            return tag;
        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing Tag read operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void update(Tag tag) throws DAOException {
        String sqlQuery = ResourceManager.valueOf(ORACLE_SQL_QUERY_UPDATE);

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {

            statement.setString(1, tag.getName());
            statement.setLong(2, tag.getId());

            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing Tag update operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void delete(long tagId) throws DAOException {
        String sqlQuery = ResourceManager.valueOf(ORACLE_SQL_QUERY_DELETE);

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {

            statement.setLong(1, tagId);

            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing Tag delete operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void deleteBindTagWithNews(long tagId) throws DAOException {
        String sqlQuery = ResourceManager.valueOf(ORACLE_SQL_QUERY_DELETE_BIND_WITH_NEWS);

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {

            statement.setLong(1, tagId);

            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing delete bind Tag with News operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

    }

    @Override
    public List<Tag> readTagsForNews(News news) throws DAOException {
        String sqlQuery = ResourceManager.valueOf(ORACLE_SQL_QUERY_READ_FOR_NEWS);
        Tag tag = null;
        List<Tag> tags = null;

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {

            statement.setLong(1, news.getId());

            ResultSet resultSet = statement.executeQuery();

            tags = new ArrayList<>();
            while (resultSet.next()) {
                tag = new Tag();
                tag.setId(resultSet.getLong(1));
                tag.setName(resultSet.getString(2));

                tags.add(tag);
            }

            return tags;
        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing read Tags for News operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public List<Tag> readTagListPerPage(long from, long to) throws DAOException {
        String sqlQuery = ResourceManager.valueOf(ORACLE_SQL_QUERY_READ_LIST_PER_PAGE);
        Tag tag = null;
        List<Tag> tags = null;

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {

            statement.setLong(1, to);
            statement.setLong(2, from);

            ResultSet resultSet = statement.executeQuery();

            tags = new ArrayList<>();
            while (resultSet.next()) {
                tag = new Tag();
                tag.setId(resultSet.getLong(1));
                tag.setName(resultSet.getString(2));

                tags.add(tag);
            }

            return tags;
        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing read Tag list per page operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public List<Tag> readTagList() throws DAOException {
        String sqlQuery = ResourceManager.valueOf(ORACLE_SQL_QUERY_READ_LIST);
        Tag tag = null;
        List<Tag> tags = null;

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {

            ResultSet resultSet = statement.executeQuery();

            tags = new ArrayList<>();
            while (resultSet.next()) {
                tag = new Tag();
                tag.setId(resultSet.getLong(1));
                tag.setName(resultSet.getString(2));

                tags.add(tag);
            }

            return tags;
        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing read Tag list operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }


}
