package by.epam.task1.dao.impl;

import by.epam.task1.dao.AuthorDAO;
import by.epam.task1.domain.Author;
import by.epam.task1.domain.News;
import by.epam.task1.exception.DAOException;
import by.epam.task1.util.ResourceManager;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.inject.Inject;
import javax.inject.Named;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by User on 13.02.2016.
 */
@Named
public class AuthorDAOImpl implements AuthorDAO {
    private static final String ORACLE_SQL_QUERY_CREATE = "oracle.create.author";
    private static final String ORACLE_SQL_QUERY_READ = "oracle.read.author";
    private static final String ORACLE_SQL_QUERY_UPDATE = "oracle.update.author";
    private static final String ORACLE_SQL_QUERY_DELETE = "oracle.delete.author";
    private static final String ORACLE_SQL_QUERY_READ_FOR_NEWS = "oracle.read.author.for.news";
    private static final String ORACLE_SQL_QUERY_READ_LIST = "oracle.read.author.list";
    private static final String ORACLE_SQL_QUERY_READ_LIST_PER_PAGE = "oracle.read.author.list.per.page";
    private static final String ORACLE_SQL_QUERY_MAKE_EXPIRED = "oracle.make.author.expire";
    private static final String AUTHOR_ID_ATTRIBUTE = "author_id";

    @Inject
    private DataSource dataSource;

    @Override
    public Long create(Author author) throws DAOException {
        String sqlQuery = ResourceManager.valueOf(ORACLE_SQL_QUERY_CREATE);
        Long authorId = null;
        String generatedKeys[] = { AUTHOR_ID_ATTRIBUTE };

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery, generatedKeys)) {

            statement.setString(1, author.getName());
            statement.executeUpdate();

            ResultSet resultSet = statement.getGeneratedKeys();

            if (resultSet.next()) {
                authorId = resultSet.getLong(1);
            }

            return authorId;
        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing Author creation operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public Author read(long authorId) throws DAOException {
        String sqlQuery = ResourceManager.valueOf(ORACLE_SQL_QUERY_READ);
        Author author = null;

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {

            statement.setLong(1, authorId);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                author = new Author();
                author.setId(resultSet.getLong(1));
                author.setName(resultSet.getString(2));
                author.setExpiredDate(resultSet.getTimestamp(3));
            }

            return author;

        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing Author read operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void update(Author author) throws DAOException {
        String sqlQuery = ResourceManager.valueOf(ORACLE_SQL_QUERY_UPDATE);

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)){

            statement.setString(1, author.getName());
            statement.setLong(2, author.getId());

            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing Author update operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

    }

    @Override
    public void delete(long authorId) throws DAOException {
        String sqlQuery = ResourceManager.valueOf(ORACLE_SQL_QUERY_DELETE);

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {

            statement.setLong(1, authorId);

            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing Author delete operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

    }

    @Override
    public void makeExpired(Author author) throws DAOException {
        String sqlQuery = ResourceManager.valueOf(ORACLE_SQL_QUERY_MAKE_EXPIRED);
        Timestamp authorExpireDate = new Timestamp(author.getExpiredDate().getTime());

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {

            statement.setTimestamp(1, authorExpireDate);
            statement.setLong(2, author.getId());

            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing make Author expire operation");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }


    @Override
    public Author readAuthorForNews(News news) throws DAOException {
        String sqlQuery = ResourceManager.valueOf(ORACLE_SQL_QUERY_READ_FOR_NEWS);
        Author author = null;

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {

            statement.setLong(1, news.getId());

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                author = new Author();
                author.setId(resultSet.getLong(1));
                author.setName(resultSet.getString(2));
                author.setExpiredDate(resultSet.getTimestamp(3));
            }

            return author;
        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing read Author for News operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public List<Author> readAuthorListPerPage(long from, long to) throws DAOException {
        String sqlQuery = ResourceManager.valueOf(ORACLE_SQL_QUERY_READ_LIST_PER_PAGE);
        Author author = null;
        List<Author> authors = null;

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {

            statement.setLong(1, to);
            statement.setLong(2, from);

            ResultSet resultSet = statement.executeQuery();

            authors = new ArrayList<>();
            while (resultSet.next()) {
                author = new Author();
                author.setId(resultSet.getLong(1));
                author.setName(resultSet.getString(2));
                author.setExpiredDate(resultSet.getTimestamp(3));
                authors.add(author);
            }

            return authors;
        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing read Author list per page operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public List<Author> readAuthorList() throws DAOException {
        String sqlQuery = ResourceManager.valueOf(ORACLE_SQL_QUERY_READ_LIST);
        Author author = null;
        List<Author> authors = null;

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {

            ResultSet resultSet = statement.executeQuery();

            authors = new ArrayList<>();
            while (resultSet.next()) {
                author = new Author();
                author.setId(resultSet.getLong(1));
                author.setName(resultSet.getString(2));
                author.setExpiredDate(resultSet.getTimestamp(3));
                authors.add(author);
            }

            return authors;
        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing read Author list operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }
}
