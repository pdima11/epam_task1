package by.epam.task1.dao.impl;

import by.epam.task1.dao.CommentDAO;
import by.epam.task1.domain.Comment;
import by.epam.task1.domain.News;
import by.epam.task1.exception.DAOException;
import by.epam.task1.util.ResourceManager;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.inject.Inject;
import javax.inject.Named;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dzmitry_Padvalnikau on 2/12/2016.
 */
@Named
public class CommentDAOImpl implements CommentDAO {
    private static final String ORACLE_SQL_QUERY_CREATE = "oracle.create.comment";
    private static final String ORACLE_SQL_QUERY_READ = "oracle.read.comment";
    private static final String ORACLE_SQL_QUERY_UPDATE = "oracle.update.comment";
    private static final String ORACLE_SQL_QUERY_DELETE = "oracle.delete.comment";
    private static final String ORACLE_SQL_QUERY_DELETE_FOR_NEWS = "oracle.delete.comment.for.news";
    private static final String ORACLE_SQL_QUERY_READ_LIST_FOR_NEWS = "oracle.read.comment.list.for.news";
    private static final String COMMENT_ID_ATTRIBUTE = "comment_id";


    @Inject
    private DataSource dataSource;

    @Override
    public Long create(Comment comment) throws DAOException {
        String sqlQuery = ResourceManager.valueOf(ORACLE_SQL_QUERY_CREATE);
        Long commentId = null;
        String generatedKeys[] = { COMMENT_ID_ATTRIBUTE };

        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement statement = connection.prepareStatement(sqlQuery, generatedKeys)) {

            statement.setLong(1, comment.getNewsId());
            statement.setString(2, comment.getText());

            statement.executeUpdate();

            ResultSet resultSet = statement.getGeneratedKeys();

            if (resultSet.next()) {
                commentId = resultSet.getLong(1);
            }

            return commentId;
        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing Comment create operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public Comment read(long commentId) throws DAOException {
        String sqlQuery = ResourceManager.valueOf(ORACLE_SQL_QUERY_READ);
        Comment comment = null;

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {

            statement.setLong(1, commentId);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                comment = new Comment();
                comment.setId(resultSet.getLong(1));
                comment.setNewsId(resultSet.getLong(2));
                comment.setText(resultSet.getString(3));
                comment.setCreationDate(resultSet.getTimestamp(4));
            }

            return comment;
        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing Comment read operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void update(Comment comment) throws DAOException {
        String sqlQuery = ResourceManager.valueOf(ORACLE_SQL_QUERY_UPDATE);

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {

            statement.setString(1, comment.getText());
            statement.setLong(2, comment.getId());

            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing Comment update operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void delete(long commentId) throws DAOException {
        String sqlQuery = ResourceManager.valueOf(ORACLE_SQL_QUERY_DELETE);

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {

            statement.setLong(1, commentId);

            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing Comment delete operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public List<Comment> readCommentListForNews(News news) throws DAOException {
        String sqlQuery = ResourceManager.valueOf(ORACLE_SQL_QUERY_READ_LIST_FOR_NEWS);
        Comment comment = null;
        List<Comment> comments = null;

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {

            statement.setLong(1, news.getId());

            ResultSet resultSet = statement.executeQuery();

            comments = new ArrayList<>();
            while (resultSet.next()) {
                comment = new Comment();
                comment.setId(resultSet.getLong(1));
                comment.setNewsId(resultSet.getLong(2));
                comment.setText(resultSet.getString(3));
                comment.setCreationDate(resultSet.getTimestamp(4));
                comments.add(comment);
            }

            return comments;
        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing read Comment list for News operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void deleteCommentsForNews(News news) throws DAOException {
        String sqlQuery = ResourceManager.valueOf(ORACLE_SQL_QUERY_DELETE_FOR_NEWS);

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {

            statement.setLong(1, news.getId());

            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing delete Comment for News operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }
}
