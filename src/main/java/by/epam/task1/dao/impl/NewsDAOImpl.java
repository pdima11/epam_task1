package by.epam.task1.dao.impl;

import by.epam.task1.dao.NewsDAO;
import by.epam.task1.domain.Author;
import by.epam.task1.domain.News;
import by.epam.task1.domain.SearchCriteriaTO;
import by.epam.task1.domain.Tag;
import by.epam.task1.exception.DAOException;
import by.epam.task1.util.ResourceManager;
import by.epam.task1.util.SearchCriteriaManager;
import oracle.jdbc.proxy.annotation.Pre;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.inject.Inject;
import javax.inject.Named;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 13.02.2016.
 */
@Named
public class NewsDAOImpl implements NewsDAO {
    private static final String ORACLE_SQL_QUERY_CREATE = "oracle.create.news";
    private static final String ORACLE_SQL_QUERY_READ = "oracle.read.news";
    private static final String ORACLE_SQL_QUERY_UPDATE = "oracle.update.news";
    private static final String ORACLE_SQL_QUERY_DELETE = "oracle.delete.news";
    private static final String ORACLE_SQL_QUERY_SEARCH_LIST_BY_TAG = "oracle.search.news.list.by.tag";
    private static final String ORACLE_SQL_QUERY_SEARCH_LIST_BY_AUTHOR = "oracle.search.news.list.by.author";
    private static final String ORACLE_SQL_QUERY_READ_LIST_SORTED_BY_COMMENTS = "oracle.read.news.list.sorted.by.comments";
    private static final String ORACLE_SQL_QUERY_COUNT_ALL = "oracle.count.all.news";
    private static final String ORACLE_SQL_QUERY_CREATE_BIND_WITH_AUTHOR = "oracle.create.bind.news.with.author";
    private static final String ORACLE_SQL_QUERY_CREATE_BIND_WITH_TAGS = "oracle.create.bind.news.with.tags";
    private static final String ORACLE_SQL_QUERY_DELETE_BIND_WITH_AUTHOR = "oracle.delete.bind.news.with.author";
    private static final String ORACLE_SQL_QUERY_DELETE_BIND_WITH_TAG = "oracle.delete.bind.news.with.tag";
    private static final String ORACLE_SQL_QUERY_READ_LIST_PER_PAGE = "oracle.read.news.list.per.page";
    private static final String NEWS_ID_ATTRIBUTE = "news_id";

    @Inject
    private DataSource dataSource;


    @Override
    public Long create(News news) throws DAOException {
        String sqlQuery = ResourceManager.valueOf(ORACLE_SQL_QUERY_CREATE);
        Long newsId = null;
        String generatedKeys[] = { NEWS_ID_ATTRIBUTE };

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery, generatedKeys)) {

            statement.setString(1, news.getTitle());
            statement.setString(2, news.getShortText());
            statement.setString(3, news.getFullText());

            statement.executeUpdate();

            ResultSet resultSet = statement.getGeneratedKeys();

            if (resultSet.next()) {
                newsId = resultSet.getLong(1);
            }

            return newsId;
        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing News create operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public News read(long newsId) throws DAOException {
        String sqlQuery = ResourceManager.valueOf(ORACLE_SQL_QUERY_READ);
        News news = null;

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {

            statement.setLong(1, newsId);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                news = new News();
                news.setId(resultSet.getLong(1));
                news.setTitle(resultSet.getString(2));
                news.setShortText(resultSet.getString(3));
                news.setFullText(resultSet.getString(4));
                news.setCreationDate(resultSet.getTimestamp(5));
                news.setModificationDate(resultSet.getDate(6));
            }

            return news;
        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing News read operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void update(News news) throws DAOException {
        String sqlQuery = ResourceManager.valueOf(ORACLE_SQL_QUERY_UPDATE);

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {

            statement.setString(1, news.getTitle());
            statement.setString(2, news.getShortText());
            statement.setString(3, news.getFullText());
            statement.setLong(4, news.getId());

            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing News update operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void delete(long newsId) throws DAOException {
        String sqlQuery = ResourceManager.valueOf(ORACLE_SQL_QUERY_DELETE);

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {

            statement.setLong(1, newsId);

            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing News delete operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public List<News> search(long from, long to, SearchCriteriaTO searchCriteria) throws DAOException{
        String sqlQuery = SearchCriteriaManager.generateSQLQuery(searchCriteria);
        News news = null;
        List<News> newsList = null;

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {
            statement.setLong(1, to);
            statement.setLong(2, from);

            ResultSet resultSet = statement.executeQuery();

            newsList = new ArrayList<>();
            while (resultSet.next()) {
                news = new News();
                news.setId(resultSet.getLong(1));
                news.setTitle(resultSet.getString(2));
                news.setShortText(resultSet.getString(3));
                news.setFullText(resultSet.getString(4));
                news.setCreationDate(resultSet.getTimestamp(5));
                news.setModificationDate(resultSet.getDate(6));

                newsList.add(news);
            }

            return newsList;
        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing search News list");
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public List<News> readNewsListSortedByComments(long from, long to) throws DAOException {
        String sqlQuery = ResourceManager.valueOf(ORACLE_SQL_QUERY_READ_LIST_SORTED_BY_COMMENTS);
        News news = null;
        List<News> newsList = null;

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {

            statement.setLong(1, to);
            statement.setLong(2, from);

            ResultSet resultSet = statement.executeQuery();

            newsList = new ArrayList<>();
            while (resultSet.next()) {
                news = new News();
                news.setId(resultSet.getLong(1));
                news.setTitle(resultSet.getString(2));
                news.setShortText(resultSet.getString(3));
                news.setFullText(resultSet.getString(4));
                news.setCreationDate(resultSet.getTimestamp(5));
                news.setModificationDate(resultSet.getDate(6));

                newsList.add(news);
            }

            return newsList;
        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing read News list sorted by comments", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }


    @Override
    public long countAllNews() throws DAOException {
        String sqlQuery = ResourceManager.valueOf(ORACLE_SQL_QUERY_COUNT_ALL);
        long newsCount = 0;

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                newsCount = resultSet.getLong(1);
            }

            return newsCount;
        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing get number of News operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }


    @Override
    public void createBindNewsWithAuthor(News news, Author author) throws DAOException {
        String sqlQuery = ResourceManager.valueOf(ORACLE_SQL_QUERY_CREATE_BIND_WITH_AUTHOR);

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {

            statement.setLong(1, news.getId());
            statement.setLong(2, author.getId());

            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing create bind News with Author operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

    }


    @Override
    public void createBindNewsWithTags(News news, List<Tag> tags) throws DAOException {
        String sqlQuery = ResourceManager.valueOf(ORACLE_SQL_QUERY_CREATE_BIND_WITH_TAGS);

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {

            for (Tag tag : tags) {
                statement.setLong(1, news.getId());
                statement.setLong(2, tag.getId());
                statement.executeUpdate();
            }

        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing create bind News with Tags operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void deleteBindNewsWithAuthor(long newsId) throws DAOException {
        String sqlQuery = ResourceManager.valueOf(ORACLE_SQL_QUERY_DELETE_BIND_WITH_AUTHOR);

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {

            statement.setLong(1, newsId);

            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing delete bind News with Author operation");
        }

    }

    @Override
    public void deleteBindNewsWithTags(News news, List<Tag> tags) throws DAOException {
        String sqlQuery = ResourceManager.valueOf(ORACLE_SQL_QUERY_DELETE_BIND_WITH_TAG);

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {

            for (Tag tag: tags) {
                statement.setLong(1, news.getId());
                statement.setLong(2, tag.getId());
                statement.executeUpdate();
            }

        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing delete bind News with Tags operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public List<News> readNewsListPerPage(long from, long to) throws DAOException {
        String sqlQuery = ResourceManager.valueOf(ORACLE_SQL_QUERY_READ_LIST_PER_PAGE);
        News news = null;
        List<News> newsList = null;

        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {

            statement.setLong(1, to);
            statement.setLong(2, from);

            ResultSet resultSet = statement.executeQuery();

            newsList = new ArrayList<>();
            while (resultSet.next()) {
                news = new News();
                news.setId(resultSet.getLong(1));
                news.setTitle(resultSet.getString(2));
                news.setShortText(resultSet.getString(3));
                news.setFullText(resultSet.getString(4));
                news.setCreationDate(resultSet.getTimestamp(5));
                news.setModificationDate(resultSet.getDate(6));

                newsList.add(news);
            }

            return newsList;
        } catch (SQLException e) {
            throw new DAOException("An error occurred while executing read News list per page operation", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }


}
