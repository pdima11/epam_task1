package by.epam.task1.service.impl;

import by.epam.task1.domain.*;
import by.epam.task1.service.AuthorService;
import by.epam.task1.service.CommentService;
import by.epam.task1.service.NewsService;
import by.epam.task1.service.TagService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(locations = { "/spring.xml" })
public class FullNewsServiceImplTest {

    @Mock
    private AuthorService authorService;

    @Mock
    private CommentService commentService;

    @Mock
    private NewsService newsService;

    @Mock
    private TagService tagService;

    @InjectMocks
    private FullNewsServiceImpl fullNewsService;

    @Test
    public void testCreate() throws Exception {
        FullNewsTO fullNews = new FullNewsTO();
        News news = new News();
        Author author= new Author();
        Tag tag = new Tag();

        List<Tag> tags = new ArrayList<>();
        tags.add(tag);

        fullNews.setNews(news);
        fullNews.setAuthor(author);
        fullNews.setTags(tags);

        fullNewsService.create(fullNews);

        verify(newsService, times(1)).create(fullNews.getNews());
        verify(newsService, times(1)).createBindWithAuthor(fullNews.getNews(), fullNews.getAuthor());
        verify(newsService, times(1)).createBindWithTags(fullNews.getNews(), fullNews.getTags());
    }

    @Test
    public void testDelete() throws Exception {
        FullNewsTO fullNews = new FullNewsTO();
        News news = new News();
        Author author= new Author();
        Tag tag = new Tag();

        List<Tag> tags = new ArrayList<>();
        tags.add(tag);

        fullNews.setNews(news);
        fullNews.setAuthor(author);
        fullNews.setTags(tags);

        fullNewsService.delete(fullNews);

        verify(newsService, times(1)).deleteBindWithTags(fullNews.getNews(), fullNews.getTags());
        verify(newsService, times(1)).deleteBindWithAuthor(fullNews.getAuthor().getId());
        verify(commentService, times(1)).deleteCommentsForNews(fullNews.getNews());
    }

    @Test
    public void testRead() throws Exception {
        Long newsId = 1L;

        News news = new News();
        news.setId(newsId);
        when(newsService.read(newsId)).thenReturn(news);

        FullNewsTO fullNews = fullNewsService.read(newsId);

        verify(newsService, times(1)).read(newsId);
        verify(authorService, times(1)).readAuthorForNews(news);
        verify(commentService, times(1)).readCommentsForNews(news);
        verify(tagService, times(1)).readTagsForNews(news);
    }

    @Test
    public void testSearch() throws Exception {
        int pageNumber = 1;
        Long newsId1 = 1L;
        Long newsId2 = 2L;

        SearchCriteriaTO searchCriteria = new SearchCriteriaTO();

        News news1 = new News();
        news1.setId(newsId1);
        News news2 = new News();
        news2.setId(newsId2);

        List<News> expectedNewsList = new ArrayList<>();
        expectedNewsList.add(news1);
        expectedNewsList.add(news2);

        when(newsService.search(pageNumber, searchCriteria)).thenReturn(expectedNewsList);
        List<FullNewsTO> fullNewsList = fullNewsService.search(pageNumber, searchCriteria);

        verify(newsService, times(1)).search(pageNumber, searchCriteria);
        for(News news: expectedNewsList) {
            verify(authorService, times(1)).readAuthorForNews(news);
            verify(commentService, times(1)).readCommentsForNews(news);
            verify(tagService, times(1)).readTagsForNews(news);
        }
    }

    @Test
    public void testReadNewsList() throws Exception {
        int pageNumber = 1;
        Long newsId1 = 1L;
        Long newsId2 = 2L;

        News news1 = new News();
        news1.setId(newsId1);
        News news2 = new News();
        news2.setId(newsId2);

        List<News> expectedNewsList = new ArrayList<>();
        expectedNewsList.add(news1);
        expectedNewsList.add(news2);

        when(newsService.readNewsListPerPage(pageNumber)).thenReturn(expectedNewsList);
        List<FullNewsTO> fullNewsList = fullNewsService.readNewsList(pageNumber);

        verify(newsService, times(1)).readNewsListPerPage(pageNumber);
        for(News news: expectedNewsList) {
            verify(authorService, times(1)).readAuthorForNews(news);
            verify(commentService, times(1)).readCommentsForNews(news);
            verify(tagService, times(1)).readTagsForNews(news);
        }
    }

    @Test
    public void testReadNewsListSortedByComments() throws Exception {
        int pageNumber = 1;
        Long newsId1 = 1L;
        Long newsId2 = 2L;

        News news1 = new News();
        news1.setId(newsId1);
        News news2 = new News();
        news2.setId(newsId2);

        List<News> expectedNewsList = new ArrayList<>();
        expectedNewsList.add(news1);
        expectedNewsList.add(news2);

        when(newsService.readNewsListSortedByComments(pageNumber)).thenReturn(expectedNewsList);
        List<FullNewsTO> fullNewsList = fullNewsService.readNewsListSortedByComments(pageNumber);

        verify(newsService, times(1)).readNewsListSortedByComments(pageNumber);
        for(News news: expectedNewsList) {
            verify(authorService, times(1)).readAuthorForNews(news);
            verify(commentService, times(1)).readCommentsForNews(news);
            verify(tagService, times(1)).readTagsForNews(news);
        }
    }
}