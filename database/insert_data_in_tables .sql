/* DEFAULT DATA FOR DATABASE*/


/*data for authors table*/ 
INSERT INTO authors(author_name) VALUES ('Frank Lampard');
INSERT INTO authors(author_name) VALUES ('Eden Hazard');
INSERT INTO authors(author_name) VALUES ('John Terry');
INSERT INTO authors(author_name) VALUES ('Petr Cheh');
INSERT INTO authors(author_name) VALUES ('Didier Drogba');
INSERT INTO authors(author_name) VALUES ('Zinedine Zidane');
INSERT INTO authors(author_name) VALUES ('Franck Ribery');
INSERT INTO authors(author_name) VALUES ('Thierry Henry');
INSERT INTO authors(author_name) VALUES ('Fernando Torres');
INSERT INTO authors(author_name) VALUES ('Juan Mata');
INSERT INTO authors(author_name) VALUES ('Harry Kane');
INSERT INTO authors(author_name) VALUES ('Karim Benzema');
INSERT INTO authors(author_name) VALUES ('Samir Nasri');
INSERT INTO authors(author_name) VALUES ('John Obi Mikel');
INSERT INTO authors(author_name) VALUES ('Nemanja Matic');
INSERT INTO authors(author_name) VALUES ('Diego Costa');
INSERT INTO authors(author_name) VALUES ('Fabien Barthez');
INSERT INTO authors(author_name) VALUES ('Kurt Zouma');
INSERT INTO authors(author_name) VALUES ('David Luiz');
INSERT INTO authors(author_name) VALUES ('Aleksandr Kerzhakov');


/*data for tags table*/
INSERT INTO tags(tag_name) VALUES ('Sport');
INSERT INTO tags(tag_name) VALUES ('Football');
INSERT INTO tags(tag_name) VALUES ('Tennis');
INSERT INTO tags(tag_name) VALUES ('Business');
INSERT INTO tags(tag_name) VALUES ('Tech');
INSERT INTO tags(tag_name) VALUES ('Science');
INSERT INTO tags(tag_name) VALUES ('Health');
INSERT INTO tags(tag_name) VALUES ('Shop');
INSERT INTO tags(tag_name) VALUES ('Travel');
INSERT INTO tags(tag_name) VALUES ('Weather');
INSERT INTO tags(tag_name) VALUES ('Culture');
INSERT INTO tags(tag_name) VALUES ('Nature');
INSERT INTO tags(tag_name) VALUES ('Music');
INSERT INTO tags(tag_name) VALUES ('Auto');
INSERT INTO tags(tag_name) VALUES ('Art');
INSERT INTO tags(tag_name) VALUES ('Food');
INSERT INTO tags(tag_name) VALUES ('Politics');
INSERT INTO tags(tag_name) VALUES ('Hockey');
INSERT INTO tags(tag_name) VALUES ('Economics');
INSERT INTO tags(tag_name) VALUES ('Education');


/*data for news table*/
INSERT INTO news(title, short_text, full_text, creation_date, modification_date)
VALUES ('Google boss highest-paid in US',
        'The chief executive of Google, Sundar Pichai, has been awarded $199m (�138m) in shares',
        'It makes him the highest-paid chief executive in the US.
Mr Pichai became chief executive of the search engine giant following the creation of its parent, Alphabet.
The founders of Google, Larry Page and Sergey Brin, have amassed fortunes of $34.6bn and $33.9bn, according to Forbes.
Mr Pichai was awarded 273,328 Alphabet shares on 3 February, worth a total of $199m, according to a filing with the US Securities and Exchange Commission.
The new award of shares takes Mr Pichais total stock value to approximately $650m.
Mr Pichais share award will vest incrementally each quarter until 2019. In other words, full control over the shares will pass to him on a gradual basis.',
        TIMESTAMP '2007-12-12 12:23:45',
        DATE '2011-07-11');       
INSERT INTO news(title, short_text, full_text, creation_date, modification_date)
VALUES ('India outpaces China economic',
        'Indias economy grew at an average rate of 7.5% in 2015, faster than the 6.9% growth in China.',
        'In recent history it has been unusual, but not unprecedented, for India to grow faster than China.
According to the IMF it happened in 1981,1989,1990 and 1999, and 2015 was the first instance in this millennium.
Indias government said growth in the October to December quarter was 7.3%, a slight drop on previous quarters which were revised sharply higher.
Even though the economy lost steam in the last quarter, its pace of expansion was faster than the growth posted by China in the same quarter.',
        CURRENT_TIMESTAMP,
        DATE '2011-07-11');
INSERT INTO news(title, short_text, full_text, creation_date, modification_date)
VALUES ('Tui Group hitfall in Turkish',
        'Tui Group has reported a 40% slump in holiday bookings to Turkey following the continued fighting.',
        'The owner of the Thomson and First Choice brands said it was "evident that there has been a significant shift in demand away from Turkey".
Instead, holidaymakers were heading to areas such as Spain and the Canaries.
Despite the fall, Tui expects underlying annual profit growth of 10%.
In a first-quarter update for the three months to 31 December, Tui Travel also said that demand for its hotels in Egypt had also been affected by fears of violence.',
        CURRENT_TIMESTAMP,
        SYSDATE); --/business
INSERT INTO news(title, short_text, full_text, creation_date, modification_date)
VALUES ('India blocks Zuckerbergs app',
        'Indias telecoms regulator has blocked Facebooks Free Basics internet service app.',
        'The scheme offered free access to a limited number of websites.
However, it was opposed by supporters of net neutrality who argued that data providers should not favour some online services over others.
The free content included selected local news and weather forecasts, the BBC, Wikipedia and some health sites.',
        CURRENT_TIMESTAMP,
        SYSDATE);
INSERT INTO news(title, short_text, full_text, creation_date, modification_date)
VALUES ('Internet monitoring bill must',
        'Plans to give firm legal backing to mass data collection and hacking.',
        'The extent of the intelligence agencies computer and internet spying operation has recently become clear.
The draft Investigatory Powers Bill is meant to put it on a firm legal footing.
But the Intelligence and Security Committee says the bill lacks clarity and is a "missed opportunity".
Committee chairman, Conservative MP Dominic Grieve, said: "We had expected to find universal privacy protections applied consistently throughout, or at least an overarching statement at the forefront of the legislation.',
        CURRENT_TIMESTAMP,
        SYSDATE); -- tech
INSERT INTO news(title, short_text, full_text, creation_date, modification_date)
VALUES ('Win gives us confidence',
        'Arsene Wenger said that Arsenal�s win over AFC Bournemouth has set them up well ahead.',
        'Arsenal are one of only two teams to have beaten the Foxes in this BPL campaign, having won 5-2 at the King Power Stadium in September.
�[Beating AFC Bournemouth] is very good for the future as we now have a very big game at home against Leicester who are now the favourites for the Premier League,� Wenger told Arsenal Player. �[A win] can maybe prepare you in a better condition for the next game.
"We have a week to prepare for that and I have to think about it. Leicester is a strong side but we are also a strong side. At home with our fans and support, we can do it."',
        CURRENT_TIMESTAMP,
        SYSDATE);
INSERT INTO news(title, short_text, full_text, creation_date, modification_date)
VALUES ('Sunderlands Greenwood',
        'Sunderland�s ambition of reaching.',
        'He was philosophical about the defeat but appreciated the experience the tournament had given him and his team-mates.
"It�s always nice to play against some different teams who play different ways so we can learn from that and improve," Greenwood told safc.com. �Hopefully we can get into a similar situation again next year in this competition because it�s good for our learning and our development.�',
        CURRENT_TIMESTAMP,
        SYSDATE);
INSERT INTO news(title, short_text, full_text, creation_date, modification_date)
VALUES ('New look for Premier League',
        'Today sees the launch of an exciting new visual identity for the Premier League which will.',
        'Working in partnership with global agency DesignStudio and Robin Brand Consultants the League has created a bold and vibrant identity that includes a modern take on the lion icon � a symbol that is part of the competition�s heritage � which is flexible in digital and broadcast formats.
The film above provides a snapshot of how the Premier League will look and feel in 2016/17, with more to come ahead of next season.',
        CURRENT_TIMESTAMP,
        SYSDATE);
INSERT INTO news(title, short_text, full_text, creation_date, modification_date)
VALUES ('Hiddink: Chelsea deserved',
        'Chelsea left it late to continue their unbeaten run under the interim management of Guus Hiddink.',
        'Diego Costa struck in the 91st minute to cancel out Jesse Lingards brilliant strike on the hour mark, meaning Chelsea have not suffered defeat in their last nine Barclays Premier League matches. 
"We deserved the point in the end," Hiddink said. "They had a beautiful goal with Lingard, but after that we reacted well, going for the equaliser."
For Hiddinks post-match reaction in full, watch the video above.',
        CURRENT_TIMESTAMP,
        SYSDATE); -- sport football
INSERT INTO news(title, short_text, full_text, creation_date, modification_date)
VALUES ('Crosby scores',
        'Four-point night ends Ducks win streak at six games',
        'PITTSBURGH - Sidney Crosby scored for a career-high seventh consecutive game and finished with four points to help the Pittsburgh Penguins to a 6-2 win against the Anaheim Ducks at Consol Energy Center on Monday. 
With the Penguins leading 3-1 late in the second period, Ducks captain Ryan Getzlaf attempted a pass from the offensive blue line that hit Crosbys leg and bounced toward center ice. Crosby corralled the puck, leading to a breakaway where he slid a snap shot through goalie John Gibsons five-hole with 4:59 remaining in the second. ',
        CURRENT_TIMESTAMP,
        SYSDATE); -- sport hockey
INSERT INTO news(title, short_text, full_text, creation_date, modification_date)
VALUES ('IQ test for dogs',
        'Scientists create IQ test for dogs... and it could help humans too',
        'If youve ever asked your dog "whos a clever boy?" there may now be a definitive way of getting the answer.
British scientists have devised what is believed to be the first IQ test for dogs, using a series of obstacles to assess the intelligence of 68 working border collies.',
        CURRENT_TIMESTAMP,
        SYSDATE); -- education tech
INSERT INTO news(title, short_text, full_text, creation_date, modification_date)
VALUES ('Cruise ship turns around',
        'Cruise ship turns around, calls off trip after storm',
        'The Anthem of the Seas has turned around and headed back to its original port after a severe storm injured four people and forced passengers into their staterooms for safety reasons, Royal Caribbean International said Monday.
Royal Caribbean said Anthem of the Seas encountered rough seas off Cape Hatteras, North Carolina, while sailing Sunday from the New York area to Port Canaveral, Florida, in what was intended originally to be a seven-day voyage to the Bahamas. It said the ship suffered damage to some public areas and cabins but "remains seaworthy." None of the injuries was serious, the cruise line said.',
        CURRENT_TIMESTAMP,
        SYSDATE); -- travell
INSERT INTO news(title, short_text, full_text, creation_date, modification_date)
VALUES ('C.Beatons mid-century socialit',
        'Englands mid-20th century socialites through the eyes of Cecil Beaton',
        'Celebrated portraitist Cecil Beaton is renowned for his images of Queen Elizabeth II and Golden Age Hollywood starlets including Elizabeth Taylor and Marilyn Monroe.
But when he wasnt photographing stars for the likes of Vanity Fair and Vogue magazines, he turned his lens on his wealthy friends, capturing the glamorous lives of Englands aristocrats and socialites.',
        CURRENT_TIMESTAMP,
        SYSDATE); -- style art
INSERT INTO news(title, short_text, full_text, creation_date, modification_date)
VALUES ('What it costs to produce oil',
        'The collapse in the price of oil has squeezed energy companies and countries that were betting.',
        'This chart was compiled using data from more than 15,000 oil fields across 20 nations. The production costs were calculated by including a mix of capital expenditures and operational expenditures. Capital expenditures included the costs involved with building oil facilities, pipelines and new wells. Operational expenditures included the costs of lifting oil out of the ground, paying employee salaries and general administrative duties.',
        CURRENT_TIMESTAMP,
        SYSDATE); -- economics
INSERT INTO news(title, short_text, full_text, creation_date, modification_date)
VALUES ('This is Teslas worst nightmare',
        'Elon Musk has bigger problems than "super rude" customers and exploding SpaceX rockets.',
        'Wall Street has grown increasingly skeptical about the company in recent weeks.
Adam Hull, an analyst with European investment bank Berenberg, initiated coverage on Tesla Wednesday with a sell rating.
Pacific Crest analyst Brad Erickson said in a report Tuesday that investors should "avoid" Tesla stock. He cited concerns about sluggish demand for the companys new Model X crossover.
And even longtime Tesla bull Adam Jonas of Morgan Stanley is suddenly feeling a little less optimistic about the electric car company. Jonas cut his price target from $450 a share to $333 on Monday.',
        CURRENT_TIMESTAMP,
        SYSDATE); -- tech science
INSERT INTO news(title, short_text, full_text, creation_date, modification_date)
VALUES ('5 sports tech ideas',
        '5 sports tech ideas for coaches and athletes',
        'As athletes strive to set new records and engineers invent new technology, their worlds are intersecting more and more.
Right now the average football player relies on lifting weights, running agility drills and pushing heavy blocking sleds to get himself ready for game day.
It works well enough, but coaches and athletes looking for smarter ways to win are quickly embracing super-sophisticated technology on courts and playing fields. ',
        CURRENT_TIMESTAMP,
        SYSDATE); -- tech science sport
INSERT INTO news(title, short_text, full_text, creation_date, modification_date)
VALUES ('Abstract art or Icelands coast',
        'Zack Secklers aerial photos of Iceland reveal an ethereal, textured world',
        'In November, with winter fast closing in, photographer Zack Seckler strapped himself into an ultra-light aircraft and headed off into squally skies over southern Iceland.
He returned with incredible images that show a different view of the wild Nordic landscapes drawing more and more visitors each year.',
        CURRENT_TIMESTAMP,
        SYSDATE); --nature health travel art
INSERT INTO news(title, short_text, full_text, creation_date, modification_date)
VALUES ('Worlds best photography winner',
        'Jellyfish and storms: 2015 Outdoor Photographer finalists announced',
        'When it comes to bragging rights, Czech photographer George Karbus has more than his share.
Not only does he have a girlfriend who joins him when he goes freediving with jellyfish, a shot he took of her swimming with the creatures has scooped him one of the top prizes in a leading photo contest.
Karbus image was named winner in the Under Exposed category of 2015 Outdoor Photographer of the Year competition.',
        TIMESTAMP '2007-12-12 12:23:45',
        DATE '2011-07-11'); --art nature travel
INSERT INTO news(title, short_text, full_text, creation_date, modification_date)
VALUES ('Sikh actor Waris Ahluwalia',
        'Sikh actor Waris Ahluwalia: Education must follow apology over turban incident',
        'The Indian-American actor and designer Waris Ahluwalia, whose plane ticket was canceled because he refused to remove his turban, accepted an apology from Aeromexico. But he is not planning on leaving Mexico City until further steps are taken.
Ahluwalia was departing Mexico City this week, but the countrys largest airline wouldnt let him board a flight home.',
        TIMESTAMP '2007-12-12 12:23:45',
        DATE '2011-07-11'); -- education travel
INSERT INTO news(title, short_text, full_text, creation_date, modification_date)
VALUES ('Tennis umpires secretly banned',
        'Tennis match-fixing: Umpires banned over gambling scam',
        'Tennis match-fixing scandal has taken a fresh twist.
Authorities revealed Tuesday that two international umpires have been banned on charges of corruption in the past year, while four others are suspended pending the completion of an investigation.
A statement sent to CNN from the International Tennis Federation (ITF) and Tennis Integrity Unit (TIU) said Kirill Parfenov, an umpire from Kazakhstan, was decertified for life in February 2015 for "contacting another official on Facebook in an attempt to manipulate the scoring of matches."',
        TIMESTAMP '2007-12-12 12:23:45',
        DATE '2011-07-11'); -- sport tennis


/*data for comments table*/
INSERT INTO comments(news_id, comment_text, creation_date)
VALUES (1, 'Hello hello', TIMESTAMP '2008-12-12 12:23:45');
INSERT INTO comments(news_id, comment_text, creation_date)
VALUES (2, 'Good, very good', TIMESTAMP '2007-12-12 09:23:45');
INSERT INTO comments(news_id, comment_text, creation_date)
VALUES (3, 'new news news', TIMESTAMP '2007-11-12 12:12:45');
INSERT INTO comments(news_id, comment_text, creation_date)
VALUES (3, 'yes yes yes', TIMESTAMP '2011-09-12 08:23:45');
INSERT INTO comments(news_id, comment_text, creation_date)
VALUES (3, 'YEEEEEEEEEEEEEEEES', TIMESTAMP '2009-12-12 12:23:45');
INSERT INTO comments(news_id, comment_text, creation_date)
VALUES (4, 'AAAAAAAAAAAAAAAAAAAAAAAa', TIMESTAMP '2009-08-09 11:45:23');
INSERT INTO comments(news_id, comment_text, creation_date)
VALUES (5, 'LOOOOOOOOOOOOOOOOOOOOOOOOL', CURRENT_TIMESTAMP);
INSERT INTO comments(news_id, comment_text, creation_date)
VALUES (6, 'ahahahaha', CURRENT_TIMESTAMP);
INSERT INTO comments(news_id, comment_text, creation_date)
VALUES (6, ')))))))', CURRENT_TIMESTAMP);
INSERT INTO comments(news_id, comment_text, creation_date)
VALUES (6, 'uahaha', CURRENT_TIMESTAMP);
INSERT INTO comments(news_id, comment_text, creation_date)
VALUES (11, 'not bad', CURRENT_TIMESTAMP);
INSERT INTO comments(news_id, comment_text, creation_date)
VALUES (11, 'hmm', CURRENT_TIMESTAMP);
INSERT INTO comments(news_id, comment_text, creation_date)
VALUES (14, 'INSERT INTO MyTable(id, value)', CURRENT_TIMESTAMP);
INSERT INTO comments(news_id, comment_text, creation_date)
VALUES (15, 'EPAM TEAM CREATES NEW PRODUCT FOR TRAVEL CLIENT', CURRENT_TIMESTAMP);
INSERT INTO comments(news_id, comment_text, creation_date)
VALUES (15, '!!!', CURRENT_TIMESTAMP);
INSERT INTO comments(news_id, comment_text, creation_date)
VALUES (17, 'NEEEEEEEEEEEEEEEEEEWS', CURRENT_TIMESTAMP);
INSERT INTO comments(news_id, comment_text, creation_date)
VALUES (17, 'NEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEWS!!!', CURRENT_TIMESTAMP);
INSERT INTO comments(news_id, comment_text, creation_date)
VALUES (18, ':)', CURRENT_TIMESTAMP);
INSERT INTO comments(news_id, comment_text, creation_date)
VALUES (19, 'reknkhveoibveiovbpbvobvwep', CURRENT_TIMESTAMP);
INSERT INTO comments(news_id, comment_text, creation_date)
VALUES (8, 'pehdfpncpfhioprhbpwinpwvbi', CURRENT_TIMESTAMP);


/*data for news_authors table*/
INSERT INTO news_authors(news_id, author_id)
VALUES (1, 12);
INSERT INTO news_authors(news_id, author_id)
VALUES (2, 8);
INSERT INTO news_authors(news_id, author_id)
VALUES (3, 7);
INSERT INTO news_authors(news_id, author_id)
VALUES (4, 18);
INSERT INTO news_authors(news_id, author_id)
VALUES (5, 12);
INSERT INTO news_authors(news_id, author_id)
VALUES (6, 11);
INSERT INTO news_authors(news_id, author_id)
VALUES (7, 20);
INSERT INTO news_authors(news_id, author_id)
VALUES (8, 1);
INSERT INTO news_authors(news_id, author_id)
VALUES (9, 3);
INSERT INTO news_authors(news_id, author_id)
VALUES (10, 9);
INSERT INTO news_authors(news_id, author_id)
VALUES (11, 14);
INSERT INTO news_authors(news_id, author_id)
VALUES (12, 17);
INSERT INTO news_authors(news_id, author_id)
VALUES (13, 17);
INSERT INTO news_authors(news_id, author_id)
VALUES (14, 7);
INSERT INTO news_authors(news_id, author_id)
VALUES (15, 1);
INSERT INTO news_authors(news_id, author_id)
VALUES (16, 9);
INSERT INTO news_authors(news_id, author_id)
VALUES (17, 16);
INSERT INTO news_authors(news_id, author_id)
VALUES (18, 5);
INSERT INTO news_authors(news_id, author_id)
VALUES (19, 6);
INSERT INTO news_authors(news_id, author_id)
VALUES (20, 2);


/*data for news_tags table*/
INSERT INTO news_tags(news_id, tag_id)
VALUES (1, 4);
INSERT INTO news_tags(news_id, tag_id)
VALUES (2, 4);
INSERT INTO news_tags(news_id, tag_id)
VALUES (3, 4);
INSERT INTO news_tags(news_id, tag_id)
VALUES (4, 5);
INSERT INTO news_tags(news_id, tag_id)
VALUES (5, 5);
INSERT INTO news_tags(news_id, tag_id)
VALUES (6, 1);
INSERT INTO news_tags(news_id, tag_id)
VALUES (6, 2);
INSERT INTO news_tags(news_id, tag_id)
VALUES (7, 1);
INSERT INTO news_tags(news_id, tag_id)
VALUES (7, 2);
INSERT INTO news_tags(news_id, tag_id)
VALUES (8, 1);
INSERT INTO news_tags(news_id, tag_id)
VALUES (8, 2);
INSERT INTO news_tags(news_id, tag_id)
VALUES (9, 1);
INSERT INTO news_tags(news_id, tag_id)
VALUES (9, 2);
INSERT INTO news_tags(news_id, tag_id)
VALUES (10, 1);
INSERT INTO news_tags(news_id, tag_id)
VALUES (10, 18);
INSERT INTO news_tags(news_id, tag_id)
VALUES (11, 5);
INSERT INTO news_tags(news_id, tag_id)
VALUES (11, 20);
INSERT INTO news_tags(news_id, tag_id)
VALUES (12, 9);
INSERT INTO news_tags(news_id, tag_id)
VALUES (13, 8);
INSERT INTO news_tags(news_id, tag_id)
VALUES (13, 15);
INSERT INTO news_tags(news_id, tag_id)
VALUES (14, 19);
INSERT INTO news_tags(news_id, tag_id)
VALUES (15, 5);
INSERT INTO news_tags(news_id, tag_id)
VALUES (15, 6);
INSERT INTO news_tags(news_id, tag_id)
VALUES (16, 5);
INSERT INTO news_tags(news_id, tag_id)
VALUES (16, 6);
INSERT INTO news_tags(news_id, tag_id)
VALUES (16, 1); 
INSERT INTO news_tags(news_id, tag_id)
VALUES (17, 7);
INSERT INTO news_tags(news_id, tag_id)
VALUES (17, 12);
INSERT INTO news_tags(news_id, tag_id)
VALUES (17, 15);
INSERT INTO news_tags(news_id, tag_id)
VALUES (17, 9);
INSERT INTO news_tags(news_id, tag_id)
VALUES (18, 9);
INSERT INTO news_tags(news_id, tag_id)
VALUES (18, 12);
INSERT INTO news_tags(news_id, tag_id)
VALUES (18, 15); 
INSERT INTO news_tags(news_id, tag_id)
VALUES (19, 20);
INSERT INTO news_tags(news_id, tag_id)
VALUES (19, 9); 
INSERT INTO news_tags(news_id, tag_id)
VALUES (20, 1);
INSERT INTO news_tags(news_id, tag_id)
VALUES (20, 3); 
