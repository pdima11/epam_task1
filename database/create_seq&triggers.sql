/*
create sequences and triggers for tables,
which have primary key
*/

-- for users table
CREATE SEQUENCE users_seq;

CREATE OR REPLACE TRIGGER users_trigger
BEFORE INSERT ON users
FOR EACH ROW
BEGIN
   IF :new.user_id IS NULL THEN
    :NEW.user_id := users_seq.NEXTVAL;
   END IF; 
END;


-- for authors table  
CREATE SEQUENCE author_seq;
  
CREATE OR REPLACE TRIGGER authors_trigger
BEFORE INSERT ON authors
FOR EACH ROW
BEGIN
  IF :new.author_id IS NULL THEN
    :NEW.author_id := author_seq.NEXTVAL;
  END IF;
END;


-- for news table
CREATE SEQUENCE news_seq;
  
CREATE OR REPLACE TRIGGER news_trigger
BEFORE INSERT ON news
FOR EACH ROW
BEGIN
  IF :new.news_id IS NULL THEN
    :NEW.news_id := news_seq.NEXTVAL;
  END IF;
END;


-- for tags table
CREATE SEQUENCE tag_seq;
  
CREATE OR REPLACE TRIGGER tags_trigger
BEFORE INSERT ON tags
FOR EACH ROW
BEGIN
  IF :new.tag_id IS NULL THEN
    :NEW.tag_id := tag_seq.NEXTVAL;
  END IF;
END;


--for comments table
CREATE SEQUENCE comments_seq;
  
CREATE OR REPLACE TRIGGER comment_trigger
BEFORE INSERT ON comments
FOR EACH ROW
BEGIN
  IF :new.comment_id IS NULL THEN
    :NEW.comment_id := comments_seq.NEXTVAL;
  END IF;
END;